﻿using System.Windows;

namespace Moving_In
{
    /// <summary>
    /// Interaction logic for PlayerView.xaml
    /// </summary>
    public partial class PlayerView
    {
        public static readonly DependencyProperty PlayerProperty = DependencyProperty.Register("Player", typeof(Player), typeof(PlayerView), new PropertyMetadata(default(Player)));

        public PlayerView()
        {
            InitializeComponent();
            AddHandler(RoomView.PileClickedEvent, new PileClickedRoutedEventHandler(PileClickedHandler));
            AddHandler(RoomView.RoomClickedEvent, new RoomClickedRoutedEventHandler(RoomClickedHandler));
        }

        public Player Player
        {
            get { return (Player) GetValue(PlayerProperty); }
            set { SetValue(PlayerProperty, value); }
        }


        // Intercept the events if the player they are for isn't the current player.

        private void PileClickedHandler(object sender, PileClickedRoutedEventArgs e)
        {
            if (Player.State != PlayerState.CurrentPlayer) e.Handled = true;
        }

        private void RoomClickedHandler(object sender, RoomClickedRoutedEventArgs e)
        {
            if (Player.State != PlayerState.CurrentPlayer) e.Handled = true;
        }
    }
}
