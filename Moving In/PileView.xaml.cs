﻿using System.Collections.Generic;
using System.Windows;

namespace Moving_In
{
    /// <summary>
    /// Interaction logic for PileView.xaml
    /// </summary>
    public partial class PileView
    {
        public PileView()
        {
            InitializeComponent();
        }

        public IEnumerable<Box> Pile
        {
            get { return (IEnumerable<Box>) GetValue(PileProperty); }
            set { SetValue(PileProperty, value); }
        }

        public static readonly DependencyProperty PileProperty =
            DependencyProperty.Register("Pile", typeof(IEnumerable<Box>), typeof(PileView), new PropertyMetadata(default(IEnumerable<Box>)));
    }
}
