using System;
using System.Globalization;
using System.Windows.Data;

namespace Moving_In
{
    public class BoolToPauseButtonLabel:IValueConverter
    {
        private static BoolToPauseButtonLabel _instance;


        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return (bool) value ? "Pause" : "Resume";
        }


        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }


        public static BoolToPauseButtonLabel Instance => _instance ?? (_instance = new BoolToPauseButtonLabel());
    }
}