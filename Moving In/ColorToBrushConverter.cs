using System;
using System.Globalization;
using System.Windows.Data;
using System.Windows.Media;

namespace Moving_In
{
    public class ColorToBrushConverter:IValueConverter
    {
        private static ColorToBrushConverter _instance;


        public object Convert(object value, Type targetType, object parameter, CultureInfo culture) =>
            value is Color ? new SolidColorBrush((Color) value) : null;


        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture) =>
            (value as SolidColorBrush)?.Color;


        public static ColorToBrushConverter Instance => _instance ?? (_instance = new ColorToBrushConverter());
    }
}