using System;
using System.Globalization;
using System.Windows;
using System.Windows.Data;

namespace Moving_In
{
    public class VisibilityIfEqualConverter:IValueConverter
    {
        private static VisibilityIfEqualConverter _instance;

        public object Convert(object value, Type targetType, object parameter, CultureInfo culture) =>
            Equals(value, parameter) ? Visibility.Visible : Visibility.Collapsed;

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }

        public static VisibilityIfEqualConverter Instance =>
            _instance ?? (_instance = new VisibilityIfEqualConverter());
    }
}