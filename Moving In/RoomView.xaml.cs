﻿using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;

namespace Moving_In
{
    /// <summary>
    ///     Interaction logic for RoomView.xaml
    /// </summary>
    public partial class RoomView
    {
        public static readonly DependencyProperty RoomProperty =
            DependencyProperty.Register("Room", typeof(Room), typeof(RoomView), new PropertyMetadata(default(Room)));

        public RoomView()
        {
            InitializeComponent();
        }

        public Room Room
        {
            get { return (Room) GetValue(RoomProperty); }
            set { SetValue(RoomProperty, value); }
        }

        public static readonly RoutedEvent RoomClickedEvent = EventManager.RegisterRoutedEvent("RoomClicked",
            RoutingStrategy.Bubble, typeof(RoomClickedRoutedEventHandler), typeof(RoomView));

        public static readonly RoutedEvent PileClickedEvent = EventManager.RegisterRoutedEvent("PileClicked",
            RoutingStrategy.Bubble, typeof(PileClickedRoutedEventHandler), typeof(RoomView));


        private void RoomOnMouseLeftButtonUp(object sender, MouseButtonEventArgs e) =>
            RaiseEvent(new RoomClickedRoutedEventArgs((sender as FrameworkElement)?.Tag as Room));


        private void PileOnMouseLeftButtonUp(object sender, MouseButtonEventArgs e) => RaiseEvent(
            new PileClickedRoutedEventArgs((sender as FrameworkElement)?.Tag as Pile));
    }


    public class RoomItemTemplateSelector:DataTemplateSelector
    {
        public override DataTemplate SelectTemplate(object item, DependencyObject container) => item is IPlace
            ? PlaceDataTemplate
            : (item is Pile ? PileDataTemplate : base.SelectTemplate(item, container));

        public DataTemplate PileDataTemplate { get; set; }

        public DataTemplate PlaceDataTemplate { get; set; }
    }

    public delegate void RoomClickedRoutedEventHandler(object sender, RoomClickedRoutedEventArgs e);

    public class RoomClickedRoutedEventArgs : RoutedEventArgs
    {
        public readonly Room Room;

        public RoomClickedRoutedEventArgs(Room room) : base(RoomView.RoomClickedEvent)
        {
            Room = room;
        }
    }

    public delegate void PileClickedRoutedEventHandler(object sender, PileClickedRoutedEventArgs e);

    public class PileClickedRoutedEventArgs : RoutedEventArgs
    {
        public readonly Pile Pile;

        public PileClickedRoutedEventArgs(Pile pile) : base(RoomView.PileClickedEvent)
        {
            Pile = pile;
        }
    }

}