﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Runtime.CompilerServices;
using System.Threading;
using System.Threading.Tasks;
using Moving_In.Annotations;

namespace Moving_In
{
    public class PausableTimer: INotifyPropertyChanged
    {
        private readonly object _lock;
        private readonly IComparer<ScheduledItem> _scheduledItemComparer;
        private readonly List<ScheduledItem> _scheduledItems;
        private TaskCompletionSource<bool> _interruptTaskCompletionSource;
        private DateTime _lastResumeTime;
        private Task _runTask;
        private TimeSpan _timeAtLastPause;
        private TaskCompletionSource<bool> _waitForChangeTaskCompletionSource;
        private bool _isRunning;
        private bool _autoPaused;
        private readonly Timer _currentTimeChangedTimer;


        public PausableTimer(bool startAutoPaused)
        {
            _lock = new object();
            _timeAtLastPause = TimeSpan.Zero;
            _lastResumeTime = DateTime.UtcNow;
            _scheduledItems = new List<ScheduledItem>();
            var timeSpanComparer = Comparer<TimeSpan>.Default;
            _scheduledItemComparer =
                Comparer<ScheduledItem>.Create(
                    (item1, item2) => timeSpanComparer.Compare(item1.TargetTime, item2.TargetTime));
            _waitForChangeTaskCompletionSource = new TaskCompletionSource<bool>();
            _runTask = null;
            UseAutoPause = _autoPaused = startAutoPaused;
            _currentTimeChangedTimer = new Timer(CurrentTimeChangedTimerCallback);
        }


        private void CurrentTimeChangedTimerCallback(object state) => OnPropertyChanged(nameof(CurrentTime));

        public bool IsPaused => !IsRunning;

        public bool IsRunning
        {
            get { return _isRunning; }
            private set
            {
                if (value == _isRunning) return;
                _isRunning = value;
                OnPropertyChanged();
                OnPropertyChanged(nameof(IsPaused));
            }
        }


        public void Pause()
        {
            lock (_lock)
            {
                _autoPaused = false;
                PauseImpl();
            }
        }


        public void Resume() => ResumeImpl();


        public void AutoPause()
        {
            if (!UseAutoPause) return;
            lock (_lock)
            {
                if (!IsRunning) return;
                _autoPaused = true;
                PauseImpl();
            }
        }


        public void AutoResume()
        {
            lock(_lock)
                if (_autoPaused)
                    ResumeImpl();
        }

        public bool UseAutoPause { get; set; }


        private void PauseImpl()
        {
            lock (_lock)
            {
                if (!IsRunning) return;
                IsRunning = false;
                _interruptTaskCompletionSource.SetResult(true);
                _timeAtLastPause += DateTime.UtcNow - _lastResumeTime;
                _currentTimeChangedTimer.Change(TimeSpan.Zero, TimeSpan.FromMilliseconds(-1));
            }
        }

        private void ResumeImpl()
        {
            lock (_lock)
            {
                if (IsRunning) return;
                IsRunning = true;
                _interruptTaskCompletionSource = CreateTaskCompletionSource();
                _lastResumeTime = DateTime.UtcNow;
                var runTask = _runTask;
                (_runTask = Task.Run(() => Run(runTask))).ContinueWith(MainWindow.ErrorHandler,
                    TaskContinuationOptions.OnlyOnFaulted);
                _currentTimeChangedTimer.Change(TimeSpan.Zero, TimeSpan.FromSeconds(0.2));
            }
        }

        private async Task Run(Task oldTask)
        {
            // Ensure that the previous run look has finished before starting this one.
            if (oldTask != null)
                await oldTask;

            while (true)
            {
                try
                {
                    ScheduledItem item = null;
                    Task task;
                    lock (_lock)
                    {
                        // If we aren't running then exit.
                        if (!IsRunning) return;
                        // If we need the task we at least should resume when the task is completed.
                        task = _interruptTaskCompletionSource.Task;
                        Debug.WriteLine(
                            $"Running - _interruptTaskCompletionSource: {_interruptTaskCompletionSource?.GetHashCode()} - {task.Status}");
                        if (_scheduledItems.Count != 0)
                        {
                            var now = CurrentTime;
                            if (_scheduledItems[0].TargetTime < now)
                            {
                                // Front item is ready to execute so grab it.
                                item = _scheduledItems[0];
                                _scheduledItems.RemoveAt(0);
                            }
                            else
                                task = Task.WhenAny(task, Task.Delay(_scheduledItems[0].TargetTime - now));
                        }
                    }

                    if (item == null)
                        // If item is null then we didn't have a task ready to trigger so need to delay till one is ready.
                        await task;
                    else
                        item.TaskCompletionSource.SetResult(true);
                }
                catch (Exception exception)
                {
                    Debug.WriteLine($"Exception in Run Loop: {exception}");
                }
            }
        }

        public TimeSpan CurrentTime
        {
            get
            {
                lock (_lock)
                    return (IsRunning ? DateTime.UtcNow - _lastResumeTime : TimeSpan.Zero) + _timeAtLastPause;
            }
        }

        public Task Delay(TimeSpan delay)
        {
            lock (_lock)
                return AtTime(CurrentTime + delay);
        }


        public Task AtTime(TimeSpan targetTime)
        {
            lock (_lock)
            {
                var scheduleItem = new ScheduledItem(targetTime);

                var index = _scheduledItems.BinarySearch(scheduleItem, _scheduledItemComparer);
                if (index < 0)
                    index = ~index;
                else
                    while (index < _scheduledItems.Count &&
                           _scheduledItems[index].TargetTime == scheduleItem.TargetTime)
                        ++index;

                _scheduledItems.Insert(index, scheduleItem);
                if (index != 0 || !IsRunning) return scheduleItem.TaskCompletionSource.Task;
                // If inserting at the front then we need to wake up the runner thread as the existing delay will be too long.
                Debug.WriteLine(
                    $"New Delay is Next Delay - _interruptTaskCompletionSource: {_interruptTaskCompletionSource.GetHashCode()} - {_interruptTaskCompletionSource.Task.Status}");
                var interruptTaskCompletionSource = _interruptTaskCompletionSource;
                _interruptTaskCompletionSource = CreateTaskCompletionSource();
                interruptTaskCompletionSource.SetResult(true);
                Debug.WriteLine(
                    $"New Delay is Next Delay after interrupt - _interruptTaskCompletionSource: {_interruptTaskCompletionSource.GetHashCode()} - {_interruptTaskCompletionSource.Task.Status}");
                return scheduleItem.TaskCompletionSource.Task;
            }
        }


        public Task WaitForChange() => _waitForChangeTaskCompletionSource.Task;


        public void NotifyOfChange() => Interlocked
            .Exchange(ref _waitForChangeTaskCompletionSource, CreateTaskCompletionSource()).SetResult(true);


        private static TaskCompletionSource<bool> CreateTaskCompletionSource() => new TaskCompletionSource<bool>(
            TaskCreationOptions.RunContinuationsAsynchronously);


        private class ScheduledItem
        {
            public readonly TimeSpan TargetTime;
            public readonly TaskCompletionSource<bool> TaskCompletionSource;

            public ScheduledItem(TimeSpan targetTime)
            {
                TargetTime = targetTime;
                TaskCompletionSource = CreateTaskCompletionSource();
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;

        [NotifyPropertyChangedInvocator]
        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null) =>
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
    }
}