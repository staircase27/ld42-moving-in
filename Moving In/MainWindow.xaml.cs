﻿using System;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Media;

namespace Moving_In
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow
    {
        public static readonly DependencyProperty ModelProperty = DependencyProperty.Register("Model", typeof(Model), typeof(MainWindow), new PropertyMetadata(default(Model),ModelPropertyChangedCallback));

        private static void ModelPropertyChangedCallback(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            var window = d as MainWindow;
            if (window == null) return;
            var oldModel = e.OldValue as Model;
            var newModel = e.NewValue as Model;
            if (oldModel != null)
                foreach (var lorry in oldModel.Lorries)
                    lorry.PropertyChanged -= window.LorryPropertyChanged;
            if (newModel != null)
                foreach (var lorry in newModel.Lorries)
                    lorry.PropertyChanged += window.LorryPropertyChanged;
        }

        private void LorryPropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            if (e.PropertyName != nameof(Lorry.State)) return;
            if (!Model.Lorries.All(lorry => lorry.State == LorryState.Completed ||
                                            lorry.State == LorryState.RanOutOfTime)) return;
            Model.Timer.Pause();
            if (new GameOverDialog(Model).ShowDialog() != true)
                Application.Current.Shutdown();
            var levelSelectorDialog = new LevelSelectorDialog();
            if(levelSelectorDialog.ShowDialog()!= true)
                Application.Current.Shutdown();
            StartLevel(levelSelectorDialog.CreateModel());
        }

        public MainWindow()
        {
            InitializeComponent();
            var levelSelectorDialog = new LevelSelectorDialog();
            if (levelSelectorDialog.ShowDialog() != true)
                Application.Current.Shutdown();
            StartLevel(levelSelectorDialog.CreateModel());
        }

        private void StartLevel(Model model)
        {
            Model = model;
            AddHandler(RoomView.PileClickedEvent, new PileClickedRoutedEventHandler(PileClickedHandler));
            AddHandler(RoomView.RoomClickedEvent, new RoomClickedRoutedEventHandler(RoomClickedHandler));
            foreach (var lorry in Model.Lorries)
                lorry.DoDeliveries().ContinueWith(ErrorHandler, TaskContinuationOptions.OnlyOnFaulted);
        }

        public static void ErrorHandler(Task obj)
        {
            Debug.WriteLine($"Error In Task: {obj.Exception}");
        }

        public Model Model
        {
            get { return (Model) GetValue(ModelProperty); }
            set { SetValue(ModelProperty, value); }
        }


        private void PileClickedHandler(object sender, PileClickedRoutedEventArgs e)
        {
            if (Model.CurrentPlayer == null) return;
            e.Handled = true;
            Model.CurrentPlayer.InteractWithPile(e.Pile);
        }

        private void RoomClickedHandler(object sender, RoomClickedRoutedEventArgs e)
        {
            if (Model.CurrentPlayer == null) return;
            e.Handled = true;
            Model.CurrentPlayer.MoveToRoom(e.Room);
        }


        private void PauseButtonOnClick(object sender, RoutedEventArgs e)
        {
            if (Model.Timer.IsRunning)
                Model.Timer.Pause();
            else Model.Timer.Resume();
        }


        private void WaitButtonOnClick(object sender, RoutedEventArgs e)
        {
            if (Model.CurrentPlayer == null) return;
            e.Handled = true;
            Model.CurrentPlayer.WaitForChange();
        }
    }
}
