﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Media;

namespace Moving_In
{
    /// <summary>
    /// Interaction logic for LevelSelectorDialog.xaml
    /// </summary>
    public partial class LevelSelectorDialog : Window
    {
        public static readonly DependencyProperty SelectedLevelTypeProperty = DependencyProperty.Register("SelectedLevelType", typeof(LevelType), typeof(LevelSelectorDialog), new PropertyMetadata(default(LevelType)));
        public static readonly DependencyProperty DifficultyProperty = DependencyProperty.Register("Difficulty", typeof(int), typeof(LevelSelectorDialog), new PropertyMetadata(default(int)));

        public LevelSelectorDialog()
        {
            var colours = new[] {Colors.MediumPurple, Colors.Red, Colors.Yellow, Colors.Orange, Colors.Blue};
            LevelTypes = new[]
            {
                new LevelType("My First Apartment (Single Room)", difficulty =>
                {
                    var r = new Random();
                    var room = new RoomProperties(2 + (difficulty - 1) / 2, colours[r.Next(colours.Length)]);
                    var boxCount = (56 + (difficulty - 1) * 14) / 8 - 2;
                    var highPileCount = (boxCount - room.PileCount) / 3;
                    var pileHeights = Enumerable.Repeat(4, highPileCount)
                        .Concat(Enumerable.Repeat(1, room.PileCount - highPileCount - 1)).Concat(new[]
                            {boxCount - (3 * highPileCount + room.PileCount - 1)});
                    var piles = pileHeights
                        .Select(count => PossiblePiles[count - 1][r.Next(PossiblePiles[count - 1].Length)]
                            .Select(type => new Box(type)).ToList())
                        .ToList();
                    Shuffle(piles, r);
                    return new Model(new LorryProperties(piles, TimeSpan.FromSeconds(3)), new[] {room},
                        new[] {new PlayerProperties(0, TimeSpan.FromSeconds(2), TimeSpan.FromSeconds(1))}, true,
                        TimeSpan.FromSeconds(
                            (int) (3 * (boxCount * (1.0 + (5.0 - difficulty) / 10.0) + 10 - difficulty))));
                }),
                new LevelType("The Luxury Of Two Rooms", difficulty =>
                    {
                        var r = new Random();
                        var room1 = new RoomProperties(2 + (difficulty - 1) / 2 - 1, colours[r.Next(colours.Length)]);
                        var room2 = new RoomProperties(2 + (difficulty - 1) / 2 + 1, colours[r.Next(colours.Length)]);
                        var boxCount = ((56 + (difficulty - 1) * 14) / 8 - 2) * 2;
                        var pileCount = room1.PileCount + room2.PileCount;
                        var highPileCount = (boxCount - pileCount) / 3;
                        var pileHeights = Enumerable.Repeat(4, highPileCount)
                            .Concat(Enumerable.Repeat(1, pileCount - highPileCount - 1)).Concat(new[]
                                {boxCount - (3 * highPileCount + pileCount - 1)});
                        var piles = pileHeights
                            .Select(count => PossiblePiles[count - 1][r.Next(PossiblePiles[count - 1].Length)]
                                .Select(type => new Box(type)).ToList())
                            .ToList();
                        Shuffle(piles, r);
                        var rooms = new[] {room1, room2}.ToList();
                        Shuffle(rooms, r);
                        return new Model(new LorryProperties(piles, TimeSpan.FromSeconds(3)), rooms,
                            new[] {new PlayerProperties(0, TimeSpan.FromSeconds(2), TimeSpan.FromSeconds(1))}, true,
                            TimeSpan.FromSeconds(
                                (int) (3 * (boxCount * (1.0 + (5.0 - difficulty) / 10.0) + 10 - difficulty))));
                    }
                ),
                new LevelType("Moving In Together (Three Rooms, Two People, Two Lorries)", difficulty =>
                    {
                        var r = new Random();
                        var room1 = new RoomProperties(2 + (difficulty - 1) / 2 - 1, colours[r.Next(colours.Length)]);
                        var room2 = new RoomProperties(2 + (difficulty - 1) / 2, colours[r.Next(colours.Length)]);
                        var room3 = new RoomProperties(2 + (difficulty - 1) / 2 + 1, colours[r.Next(colours.Length)]);
                        var boxCount = ((56 + (difficulty - 1) * 14) / 8 - 2) * 3;
                        var pileCount = room1.PileCount + room2.PileCount + room3.PileCount;
                        var highPileCount = (boxCount - pileCount) / 3;
                        var pileHeights = Enumerable.Repeat(4, highPileCount)
                            .Concat(Enumerable.Repeat(1, pileCount - highPileCount - 1)).Concat(new[]
                                {boxCount - (3 * highPileCount + pileCount - 1)});
                        var piles = pileHeights
                            .Select(count => PossiblePiles[count - 1][r.Next(PossiblePiles[count - 1].Length)]
                                .Select(type => new Box(type)).ToList())
                            .ToList();
                        Shuffle(piles, r);
                        var rooms = new[] {room1, room2, room3}.ToList();
                        Shuffle(rooms, r);
                        return new Model(new LorryProperties(piles.Take(pileCount / 2), TimeSpan.FromSeconds(3)), rooms,
                            new LorryProperties(piles.Skip(pileCount / 2), TimeSpan.FromSeconds(3)),
                            new[]
                            {
                                new PlayerProperties(0, TimeSpan.FromSeconds(2), TimeSpan.FromSeconds(1)),
                                new PlayerProperties(2, TimeSpan.FromSeconds(2), TimeSpan.FromSeconds(1))
                            }, true,
                            TimeSpan.FromSeconds(
                                // ReSharper disable once PossibleLossOfFraction
                                (int) (3 * (boxCount * (1.0 + (5.0 - difficulty) / 10.0) + 10 - difficulty)) / 2));
                    }
                )
            };
            InitializeComponent();
        }

        private static readonly BoxType[][][] PossiblePiles =
        {
            new[] {new[] {BoxType.Fragile}, new[] {BoxType.Heavy}, new[] {BoxType.Normal}},
            new[]
            {
                new[] {BoxType.Normal, BoxType.Normal},
                new[] {BoxType.Heavy, BoxType.Heavy},
            },
            new[]
            {
                new[] {BoxType.Normal, BoxType.Normal, BoxType.Fragile, },
                new[] {BoxType.Heavy, BoxType.Normal, BoxType.Fragile, },
                new[] {BoxType.Heavy, BoxType.Heavy, BoxType.Fragile, },
            },
            new[]
            {
                new[]
                {
                    BoxType.Heavy, BoxType.Normal, BoxType.Normal, BoxType.Fragile
                },
                new[]
                {
                    BoxType.Heavy, BoxType.Heavy, BoxType.Normal, BoxType.Fragile
                },
                new[]
                {
                    BoxType.Heavy, BoxType.Heavy, BoxType.Heavy, BoxType.Fragile
                },
            }
        };

        public static void Shuffle<T>(IList<T> list, Random rng)
        {
            var n = list.Count;
            while (n > 1)
            {
                n--;
                var k = rng.Next(n + 1);
                var value = list[k];
                list[k] = list[n];
                list[n] = value;
            }
        }

        public IEnumerable<LevelType> LevelTypes { get; }

        public LevelType SelectedLevelType
        {
            get { return (LevelType) GetValue(SelectedLevelTypeProperty); }
            set { SetValue(SelectedLevelTypeProperty, value); }
        }

        public int Difficulty
        {
            get { return (int) GetValue(DifficultyProperty); }
            set { SetValue(DifficultyProperty, value); }
        }

        public Model CreateModel()
        {
            return SelectedLevelType.CreateLevel(Difficulty);
        }

        private void ButtonBase_OnClick(object sender, RoutedEventArgs e)
        {
            DialogResult = true;
        }
    }

    public class LevelType
    {
        public readonly Func<int,Model> CreateLevel;

        public LevelType(string name, Func<int, Model> createLevel)
        {
            Name = name;
            CreateLevel = createLevel;
        }

        public string Name { get; }
    }
}
