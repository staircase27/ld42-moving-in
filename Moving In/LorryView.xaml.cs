﻿using System.Windows;

namespace Moving_In
{
    /// <summary>
    /// Interaction logic for LorryView.xaml
    /// </summary>
    public partial class LorryView
    {
        public static readonly DependencyProperty LorryProperty = DependencyProperty.Register("Lorry", typeof(Lorry),
            typeof(LorryView), new PropertyMetadata(default(Lorry)));


        public LorryView()
        {
            InitializeComponent();
        }


        public Lorry Lorry
        {
            get { return (Lorry) GetValue(LorryProperty); }
            set { SetValue(LorryProperty, value); }
        }
    }
}
