using System;
using System.Globalization;
using System.Windows;
using System.Windows.Data;

namespace Moving_In
{
    public class VisibilityIfNotNullConverter:IValueConverter
    {
        private static VisibilityIfNotNullConverter _instance;
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture) => value == null ? Visibility.Hidden : Visibility.Visible;

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
        public static VisibilityIfNotNullConverter Instance =>
            _instance ?? (_instance = new VisibilityIfNotNullConverter());
    }
}