﻿using System;
using System.Linq;
using System.Windows;

namespace Moving_In
{
    /// <summary>
    /// Interaction logic for GameOverDialog.xaml
    /// </summary>
    public partial class GameOverDialog
    {

        public GameOverDialog(Model model)
        {
            Model = model;
            LorriesTimesOut = model.Lorries.Count(lorry => lorry.State == LorryState.RanOutOfTime);
            TimeLeft = model.TimeLimit - model.Timer.CurrentTime;
            BottomMessage = model.LostBoxes.LostBoxes > 0
                ? "Oh dear you lost some boxes. Try and do better next time!"
                : "Congratulations! You did it!";
            InitializeComponent();
        }

        public TimeSpan TimeLeft { get; }

        public Model Model { get;  }

        public int LorriesTimesOut { get; }

        public string BottomMessage { get; }

        private void NewLevelButtonOnClick(object sender, RoutedEventArgs e) => DialogResult = true;
    }
}
