﻿// © Copyright Meyertech Ltd. 2007-2018
// 
// All rights are reserved. Reproduction or transmission in whole or in part, in
// any form or by any means, electronic, mechanical or otherwise, is prohibited
// without the prior written consent of the copyright owner.
// 
// Solution: Moving In
// Project: Moving In
// 
// Last edited by Simon Armstrong:
// 2018 - 08 - 13  @ 17:19


using System;
using System.Globalization;
using System.Windows.Data;

namespace Moving_In
{
    public class TimeSpanCountDownValueConverter:IMultiValueConverter
    {
        private static TimeSpanCountDownValueConverter _instance;


        public static TimeSpanCountDownValueConverter Instance =>
            _instance ?? (_instance = new TimeSpanCountDownValueConverter());

        public object Convert(object[] values, Type targetType, object parameter, CultureInfo culture)
        {
            if (values.Length != 2) return null;
            var timeSpan = values[0] as TimeSpan?;
            var allowedTimespan = values[1] as TimeSpan?;
            if (timeSpan == null || allowedTimespan == null) return null;
            return allowedTimespan - timeSpan;
        }


        public object[] ConvertBack(object value, Type[] targetTypes, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}