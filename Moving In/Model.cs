﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Media;
using Moving_In.Annotations;

namespace Moving_In
{
    public class Model
    {
        private readonly List<CurrentPlayerQueueItem> _currentPlayerQueue;

        public Model(LorryProperties leftLorry, IEnumerable<RoomProperties> rooms,
            IEnumerable<PlayerProperties> players, bool startAutoPaused, TimeSpan timeLimit) : this(leftLorry, rooms, null, players, startAutoPaused, timeLimit)
        {
        }

        public Model(IEnumerable<RoomProperties> rooms, LorryProperties rightLorry,
            IEnumerable<PlayerProperties> players, bool startAutoPaused, TimeSpan timeLimit) : this(null, rooms, rightLorry, players, startAutoPaused, timeLimit)
        {
        }

        public Model(LorryProperties leftLorry, IEnumerable<RoomProperties> rooms, LorryProperties rightLorry,
            IEnumerable<PlayerProperties> players, bool startAutoPaused, TimeSpan timeLimit)
        {
            TimeLimit = timeLimit;
            Timer = new PausableTimer(startAutoPaused);
            LostBoxes = new LostBoxCounter();
            _currentPlayerQueue = new List<CurrentPlayerQueueItem>();
            var tmp = rooms.ToList();
            var places = Enumerable.Repeat(0, tmp.Count).Select(i => new PlaceImpl(false)).ToList();
            places.Insert(0, leftLorry == null ? null : new PlaceImpl(true));
            places.Add(rightLorry == null ? null : new PlaceImpl(true));
            Rooms = tmp.Zip(
                    places.Skip(2).Zip(places.Skip(1), (place1, place2) => new {RightPlace = place1, Place = place2})
                        .Zip(places, (place1, place2) => new {place1.RightPlace, place1.Place, LeftPlace = place2}),
                    (properties, roomPlaces) =>
                    {
                        var room = new Room(properties.PileCount, roomPlaces.LeftPlace, roomPlaces.RightPlace,
                            properties.Color);
                        roomPlaces.Place.Room = room;
                        return room;
                    }).ToList()
                .AsReadOnly();
            var lorries = new List<Lorry>();
            if (leftLorry != null)
            {
                var lorry = new Lorry(leftLorry.BoxesToDeliver, Rooms[0].Piles[0], leftLorry.DelayBetweenDeliveries,
                    Timer, TimeLimit, LostBoxes);
                lorries.Add(lorry);
                places[0].Lorry = lorry;
            }
            if (rightLorry != null)
            {
                var lorry = new Lorry(rightLorry.BoxesToDeliver,
                    Rooms[Rooms.Count - 1].Piles[Rooms[Rooms.Count - 1].Piles.Count - 1],
                    rightLorry.DelayBetweenDeliveries, Timer, TimeLimit, LostBoxes);
                lorries.Add(lorry);
                places[places.Count - 1].Lorry = lorry;
            }
            Lorries = lorries.AsReadOnly();
            Places = places.Where(place => place != null).ToList().AsReadOnly();
            Players = players.Select(properties => new Player(Timer, Rooms[properties.InitialRoom], SetCurrentPlayer,
                ReleaseCurrentPlayer, properties.MovingRoomDelay, properties.BoxInteractionDelay, LostBoxes)).ToList().AsReadOnly();
        }


        public LostBoxCounter LostBoxes { get; }
        public IReadOnlyList<IPlace> Places { get; }
        public IReadOnlyList<Lorry> Lorries { get; }
        public IReadOnlyList<Room> Rooms { get; }
        public IReadOnlyList<Player> Players { get; }
        public Player CurrentPlayer { get; set; }
        public PausableTimer Timer { get; }

        public static Type PileType => typeof(PlaceImpl);

        public TimeSpan TimeLimit { get; }


        private Task SetCurrentPlayer(Player player)
        {
            lock (_currentPlayerQueue)
            {
                if (CurrentPlayer == null)
                {
                    CurrentPlayer = player;
                    return Task.CompletedTask;
                }
                var queueItem = new CurrentPlayerQueueItem(player);
                _currentPlayerQueue.Add(queueItem);
                return queueItem.TaskCompletionSource.Task;
            }
        }

        private void ReleaseCurrentPlayer()
        {
            lock (_currentPlayerQueue)
            {
                if (_currentPlayerQueue.Count == 0)
                {
                    CurrentPlayer = null;
                }
                else
                {
                    CurrentPlayer = _currentPlayerQueue[0].Player;
                    _currentPlayerQueue[0].TaskCompletionSource.SetResult(true);
                    _currentPlayerQueue.RemoveAt(0);
                }
            }
        }

        private class CurrentPlayerQueueItem
        {
            public readonly Player Player;

            public CurrentPlayerQueueItem(Player player)
            {
                Player = player;
                TaskCompletionSource = new TaskCompletionSource<bool>();
            }

            public TaskCompletionSource<bool> TaskCompletionSource { get; }
        }

        private class PlaceImpl : IPlace
        {
            public PlaceImpl(bool isLorry)
            {
                IsLorry = isLorry;
            }

            public Room Room
            {
                get { return Place as Room; }
                set
                {
                    if (IsLorry) throw new InvalidOperationException("Expected Lorry for this place.");
                    if (Place != null) throw new InvalidOperationException("Place already set.");
                    Place = value;
                }
            }

            public Lorry Lorry
            {
                get { return Place as Lorry; }
                set
                {
                    if (!IsLorry) throw new InvalidOperationException("Expected Lorry for this place.");
                    if (Place != null) throw new InvalidOperationException("Place already set.");
                    Place = value;
                }
            }

            public object Place { get; private set; }

            public bool IsLorry { get; }
        }
    }


    public class RoomProperties
    {
        public RoomProperties(int pileCount, Color color)
        {
            PileCount = pileCount;
            Color = color;
        }


        public int PileCount { get; }

        public Color Color { get; }
    }


    public class PlayerProperties
    {
        public PlayerProperties(int initialRoom, TimeSpan movingRoomDelay, TimeSpan boxInteractionDelay)
        {
            InitialRoom = initialRoom;
            MovingRoomDelay = movingRoomDelay;
            BoxInteractionDelay = boxInteractionDelay;
        }

        public int InitialRoom { get; }
        public TimeSpan MovingRoomDelay { get;  }
        public TimeSpan BoxInteractionDelay { get;}
    }

    public interface IPlace
    {
        Room Room { get; }
        Lorry Lorry { get; }
        object Place { get; }
        bool IsLorry { get; }
    }


    public class Room
    {
        private readonly IPlace _leftPlace;
        private readonly IPlace _rightPlace;


        public Room(int pileCount, IPlace leftPlace, IPlace rightPlace, Color colour)
        {
            Colour = colour;
            Piles = Enumerable.Range(0, pileCount)
                .Select(i => new Pile(i == 0 && leftPlace != null && leftPlace.IsLorry ||
                                      i == pileCount - 1 && rightPlace != null && rightPlace.IsLorry)).ToList()
                .AsReadOnly();
            var o = new List<object>(Piles);
            if (leftPlace != null)
                o.Insert(0, leftPlace);
            if (rightPlace != null)
                o.Add(rightPlace);
            _leftPlace = leftPlace;
            _rightPlace = rightPlace;
            Objects = o.AsReadOnly();
        }


        public IReadOnlyList<object> Objects { get; set; }

        public IReadOnlyList<Pile> Piles { get; }
        public Room LeftRoom => _leftPlace?.Room;
        public Room RightRoom => _rightPlace?.Room;
        public Color Colour { get; }
    }


    public class Pile : IReadOnlyList<Box>, INotifyCollectionChanged
    {
        private readonly ObservableCollection<Box> _boxes;

        public Pile(bool deliveryPile)
        {
            DeliveryPile = deliveryPile;
            _boxes = new ObservableCollection<Box>();
            BoxesToDeliverOnceSafe = new List<BoxToDeliverOnceSafeTask>();
        }

        public bool DeliveryPile { get; }

        private List<BoxToDeliverOnceSafeTask> BoxesToDeliverOnceSafe { get; }

        public event NotifyCollectionChangedEventHandler CollectionChanged
        {
            add { _boxes.CollectionChanged += value; }
            remove { _boxes.CollectionChanged -= value; }
        }

        public IEnumerator<Box> GetEnumerator() => _boxes.GetEnumerator();

        IEnumerator IEnumerable.GetEnumerator() => ((IEnumerable) _boxes).GetEnumerator();

        public int Count => _boxes.Count;

        public Box this[int index] => _boxes[index];

        public Box RemoveBox()
        {
            if (_boxes.Count == 0) return null;
            var box = _boxes[_boxes.Count - 1];
            _boxes.RemoveAt(_boxes.Count - 1);
            while (BoxesToDeliverOnceSafe.Count > 0 && CanAddBoxSafely(BoxesToDeliverOnceSafe[0].Box))
            {
                lock (BoxesToDeliverOnceSafe[0].CompletionTask)
                {
                    if (!BoxesToDeliverOnceSafe[0].CompletionTask.Task.IsCompleted)
                    {
                        _boxes.Add(BoxesToDeliverOnceSafe[0].Box);
                        BoxesToDeliverOnceSafe[0].CompletionTask.SetResult(true);
                    }
                }

                BoxesToDeliverOnceSafe.RemoveAt(0);
            }
            return box;
        }

        private bool CanAddBoxSafely(Box box)
        {
            return _boxes.Count <= 0 || _boxes.Count < 4 &&
                   (box.Type != BoxType.Heavy || _boxes[_boxes.Count - 1].Type == BoxType.Heavy) &&
                   _boxes[_boxes.Count - 1].Type != BoxType.Fragile &&
                   (_boxes.Count != 3 || _boxes[0].Type == BoxType.Heavy);
        }

        public int AddBox(Box box)
        {
            var broken = 0;
            if (_boxes.Count > 0)
                if (box.Type == BoxType.Heavy && _boxes[_boxes.Count - 1].Type != BoxType.Heavy)
                    do
                    {
                        BrokenBoxAt(ref broken, _boxes.Count - 1);
                    } while (_boxes.Count > 0 && _boxes[_boxes.Count - 1].Type != BoxType.Heavy);
                else if (_boxes[_boxes.Count - 1].Type == BoxType.Fragile)
                    BrokenBoxAt(ref broken, _boxes.Count - 1);
                else if (_boxes.Count >= 4)
                    BrokenBoxAt(ref broken, 0);
                else if (_boxes.Count == 3 && _boxes[0].Type != BoxType.Heavy)
                    BrokenBoxAt(ref broken, 0);
            _boxes.Add(box);
            return broken;
        }

        public Task AddBoxOnceSafe(Box box, Task cancallationTask)
        {
            if (CanAddBoxSafely(box))
            {
                _boxes.Add(box);
                return Task.CompletedTask;
            }
            var task = new BoxToDeliverOnceSafeTask(box, cancallationTask);
            BoxesToDeliverOnceSafe.Add(task);
            return task.CompletionTask.Task;
        }

        private void BrokenBoxAt(ref int broken, int index)
        {
            ++broken;
            _boxes.RemoveAt(index);
        }

        private class BoxToDeliverOnceSafeTask
        {
            public BoxToDeliverOnceSafeTask(Box box, Task cancellationTask)
            {
                Box = box;
                CompletionTask = new TaskCompletionSource<bool>();
                cancellationTask.ContinueWith(task =>
                {
                    lock (CompletionTask)
                        if (!CompletionTask.Task.IsCompleted)
                            CompletionTask.SetCanceled();
                });
            }

            public TaskCompletionSource<bool> CompletionTask { get; }
            public Box Box { get; }
        }
    }

    public class Box
    {
        public Box(BoxType type)
        {
            Type = type;
        }

        public BoxType Type { get; }
    }

    public enum BoxType
    {
        Normal = 0,
        Heavy = 1,
        Fragile = 2
    }

    public class Player : INotifyPropertyChanged
    {
        private readonly Action _releaseCurrentPlayer;
        private readonly Func<Player, Task> _setCurrentPlayer;
        private readonly PausableTimer _timer;
        [CanBeNull] private Box _carriedBox;
        [NotNull] private Room _currentRoom;
        private int _state;
        private readonly TimeSpan _movingRoomDelay;
        private readonly TimeSpan _boxInteractionDelay;
        private readonly LostBoxCounter _lostBoxes;


        public Player(PausableTimer timer, Room initialRoom, Func<Player, Task> setCurrentPlayer,
            Action releaseCurrentPlayer, TimeSpan movingRoomDelay, TimeSpan boxInteractionDelay,
            LostBoxCounter lostBoxes)
        {
            _timer = timer;
            _currentRoom = initialRoom;
            _setCurrentPlayer = setCurrentPlayer;
            _releaseCurrentPlayer = releaseCurrentPlayer;
            _movingRoomDelay = movingRoomDelay;
            _boxInteractionDelay = boxInteractionDelay;
            _lostBoxes = lostBoxes;
            _state = (int) PlayerState.WaitingForIntructions;
            WaitToBeCurrentPlayer().ContinueWith(MainWindow.ErrorHandler,
                TaskContinuationOptions.OnlyOnFaulted);
        }

        private async Task WaitToBeCurrentPlayer()
        {
            await _setCurrentPlayer(this);
            State = PlayerState.CurrentPlayer;
            _timer.AutoPause();
        }

        [CanBeNull]
        public Box CarriedBox
        {
            get { return _carriedBox; }
            private set
            {
                if (Equals(value, _carriedBox)) return;
                _carriedBox = value;
                OnPropertyChanged();
            }
        }

        [NotNull]
        public Room CurrentRoom
        {
            get { return _currentRoom; }
            private set
            {
                if (Equals(value, _currentRoom)) return;
                _currentRoom = value;
                OnPropertyChanged();
            }
        }

        public PlayerState State
        {
            get { return (PlayerState) _state; }
            private set
            {
                if (value == (PlayerState) _state) return;
                _state = (int) value;
                OnPropertyChanged();
            }
        }


        public async void WaitForChange()
        {
            if ((PlayerState) Interlocked.CompareExchange(ref _state, (int) PlayerState.WaitingForChange,
                    (int) PlayerState.CurrentPlayer) != PlayerState.CurrentPlayer) return;
            OnPropertyChanged(nameof(State));
            _releaseCurrentPlayer();
            _timer.AutoResume();
            await _timer.WaitForChange();
            State = PlayerState.WaitingForIntructions;
            await WaitToBeCurrentPlayer();
        }

        public async void MoveToRoom(Room room)
        {
            if (room == null || room != CurrentRoom.LeftRoom && room != CurrentRoom.RightRoom) return;
            if ((PlayerState)Interlocked.CompareExchange(ref _state, (int)PlayerState.Active,
                    (int)PlayerState.CurrentPlayer) != PlayerState.CurrentPlayer) return;
            OnPropertyChanged(nameof(State));
            _releaseCurrentPlayer();
            _timer.AutoResume();
            await _timer.Delay(_movingRoomDelay);
            CurrentRoom = room;
            State = PlayerState.WaitingForIntructions;
            await WaitToBeCurrentPlayer();
        }

        public async void InteractWithPile(Pile pile)
        {
            if (pile == null || !CurrentRoom.Piles.Contains(pile)) return;
            if ((PlayerState) Interlocked.CompareExchange(ref _state, (int) PlayerState.Active,
                    (int) PlayerState.CurrentPlayer) != PlayerState.CurrentPlayer) return;
            OnPropertyChanged(nameof(State));
            _releaseCurrentPlayer();
            await _timer.Delay(_boxInteractionDelay);
            if (CarriedBox == null)
                CarriedBox = pile.RemoveBox();
            else
            {
                _lostBoxes.AddLostBoxes(pile.AddBox(CarriedBox));
                CarriedBox = null;
            }
            _timer.NotifyOfChange();
            State = PlayerState.WaitingForIntructions;
            await WaitToBeCurrentPlayer();
        }

        public event PropertyChangedEventHandler PropertyChanged;

        [NotifyPropertyChangedInvocator]
        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null) =>
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
    }

    public enum PlayerState
    {
        CurrentPlayer,
        WaitingForIntructions,
        WaitingForChange,
        Active
    }

    public class Lorry : INotifyPropertyChanged
    {
        private readonly List<ObservableCollection<Box>> _boxesToDeliver;
        private readonly PausableTimer _timer;
        private readonly TimeSpan _timeLimit;
        private readonly LostBoxCounter _lostBoxes;
        private int _state;

        public Lorry(IEnumerable<IEnumerable<Box>> boxesToDeliver, Pile deliveryPile, TimeSpan delayBetweenDeliveries,
            PausableTimer timer, TimeSpan timeLimit, LostBoxCounter lostBoxes)
        {
            _deliveryPile = deliveryPile;
            DelayBetweenDeliveries = delayBetweenDeliveries;
            _timer = timer;
            _timeLimit = timeLimit;
            _lostBoxes = lostBoxes;
            _state = (int) LorryState.NotStarted;
            _boxesToDeliver = boxesToDeliver
                .Select(boxes => new ObservableCollection<Box>(boxes.Where(box => box != null)))
                .Where(boxes => boxes.Count > 0).ToList();
            if (!_boxesToDeliver.Any())
                _boxesToDeliver = new List<ObservableCollection<Box>> {new ObservableCollection<Box>()};
            BoxesToDeliver = _boxesToDeliver.Sum(boxes => boxes.Count);
            AllDelivered = false;
        }

        public IEnumerable<Box> CurrentBoxesToDeliver => _boxesToDeliver[0];

        public int BoxesToDeliver { get; private set; }

        public bool AllDelivered { get; private set; }

        public TimeSpan DelayBetweenDeliveries { get; }

        private readonly Pile _deliveryPile;

        public LorryState State => (LorryState) _state;

        public event PropertyChangedEventHandler PropertyChanged;

        private Box DeliverBoxAndGetNext()
        {
            BoxesToDeliver--;
            OnPropertyChanged(nameof(BoxesToDeliver));
            var count = _boxesToDeliver[0].Count;
            _boxesToDeliver[0].RemoveAt(count - 1);
            if (count != 1)
                return _boxesToDeliver[0][count - 2];
            if (_boxesToDeliver.Count > 1)
            {
                _boxesToDeliver.RemoveAt(0);
                OnPropertyChanged(nameof(CurrentBoxesToDeliver));
                return _boxesToDeliver[0][_boxesToDeliver[0].Count - 1];
            }

            AllDelivered = true;
            OnPropertyChanged(nameof(AllDelivered));
            return null;
        }

        public async Task DoDeliveries()
        {
            // Can't be run more than once.
            if ((LorryState) Interlocked.CompareExchange(ref _state, (int) LorryState.Working,
                    (int) LorryState.NotStarted) != LorryState.NotStarted) return;
            var cancellationTask = _timer.AtTime(_timeLimit);
            OnPropertyChanged(nameof(State));
            var box = _boxesToDeliver[0][_boxesToDeliver[0].Count - 1];
            while (true)
            {
                _state = (int) LorryState.Waiting;
                OnPropertyChanged(nameof(State));
                try
                {
                    await _deliveryPile.AddBoxOnceSafe(box, cancellationTask).ConfigureAwait(true);
                }
                catch (OperationCanceledException)
                {
                    if (cancellationTask.IsCompleted) break;
                }
                box = DeliverBoxAndGetNext();
                _timer.NotifyOfChange();
                _state = (int) LorryState.Working;
                OnPropertyChanged(nameof(State));
                if (box == null)
                {
                    AllDelivered = true;
                    _state = (int) LorryState.Completed;
                    OnPropertyChanged(nameof(AllDelivered));
                    OnPropertyChanged(nameof(State));
                    return;
                }

                if (await Task.WhenAny(cancellationTask, _timer.Delay(DelayBetweenDeliveries)) ==
                    cancellationTask) break;
            }

            _lostBoxes.AddLostBoxes(BoxesToDeliver);
            while (_boxesToDeliver.Count > 1)
                _boxesToDeliver.RemoveAt(0);
            _boxesToDeliver[0].Clear();
            _state = (int)LorryState.RanOutOfTime;
            OnPropertyChanged(nameof(CurrentBoxesToDeliver));
            OnPropertyChanged(nameof(AllDelivered));
            OnPropertyChanged(nameof(State));
        }

        [NotifyPropertyChangedInvocator]
        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null) =>
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
    }

    public class LorryProperties
    {
        public readonly IEnumerable<IEnumerable<Box>> BoxesToDeliver;
        public readonly TimeSpan DelayBetweenDeliveries;

        public LorryProperties(IEnumerable<IEnumerable<Box>> boxesToDeliver, TimeSpan delayBetweenDeliveries)
        {
            BoxesToDeliver = boxesToDeliver;
            DelayBetweenDeliveries = delayBetweenDeliveries;
        }
    }

    public enum LorryState
    {
        NotStarted,
        Working,
        Waiting,
        Completed,
        RanOutOfTime
    }


    public class LostBoxCounter:INotifyPropertyChanged
    {
        private int _lostBoxes;

        public int LostBoxes => _lostBoxes;

        public event PropertyChangedEventHandler PropertyChanged;


        [NotifyPropertyChangedInvocator]
        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }


        public void AddLostBoxes(int boxes)
        {
            if (boxes == 0) return;
            _lostBoxes += boxes;
            OnPropertyChanged();
        }
    }
}